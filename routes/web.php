<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//** Halaman Utama **//
Route::get('/', 'HomeController@index');
Route::get('/home/produk', 'HomeController@produk');
Route::get('/home/artikel', 'HomeController@artikel');
Route::get('/blog/{slug}', 'HomeController@selengkapnya');
Route::get('/home/kontak', 'HomeController@kontak');
Route::post('/kontak/kirim', 'HomeController@kontakKirim');
//** End Halaman Utama **//

//Sistem Login
Route::get('/login', 'AuthController@login')->name('login');
Route::post('/postlogin', 'AuthController@postlogin');
Route::get('/logout', 'AuthController@logout');

//** Halaman Admin **//
//Produk
Route::group(['middleware' => 'auth'],function(){
	Route::get('/admin', 'AdminController@index');
	Route::get('/produk', 'AdminController@produk');
	Route::post('/produk/produkCreate', 'AdminController@produkCreate');
	Route::post('/produk/produkEdit/{id}', 'AdminController@produkEdit');
	Route::post('/produk/produkDelete/{id}', 'AdminController@produkDelete');
});

//Galeri
Route::group(['middleware' => 'auth'],function(){
	Route::get('/galeri', 'AdminController@galeri');
	Route::post('/galeri/galeriCreate', 'AdminController@galeriCreate');
	Route::post('/galeri/galeriEdit/{id}', 'AdminController@galeriEdit');
	Route::post('/galeri/galeriDelete/{id}', 'AdminController@galeriDelete');
});

//Promo
Route::group(['middleware' => 'auth'],function(){
	Route::get('/promo', 'AdminController@promo');
	Route::post('/promo/promoCreate', 'AdminController@promoCreate');
	Route::post('/promo/promoEdit/{id}', 'AdminController@promoEdit');
	Route::post('/promo/promoDelete/{id}', 'AdminController@promoDelete');
});

//Testimoni
Route::group(['middleware' => 'auth'], function(){
	Route::get('/testimoni', 'AdminController@testimoni');
	Route::post('/testimoni/testimoniCreate', 'AdminController@testimoniCreate');
	Route::post('/testimoni/testimoniEdit/{id}', 'AdminController@testimoniEdit');
	Route::post('/testimoni/testimoniDelete/{id}', 'AdminController@testimoniDelete');
});

//Pesan
Route::group(['middleware' => 'auth'], function(){
	Route::get('/pesan', 'AdminController@pesan');
	Route::post('/pesan/pesanDelete/{id}', 'AdminController@pesanDelete');
});

//Admin
Route::group(['middleware' => 'auth'],function(){
	Route::get('/user', 'AdminController@user');
	Route::post('/user/userCreate', 'AdminController@userCreate');
	Route::post('/user/userEdit/{id}', 'AdminController@userEdit');
	Route::post('/user/userDelete/{id}', 'AdminController@userDelete');
});

//Artikel
Route::group(['middleware' => 'auth'], function(){
	Route::get('/artikel', 'AdminController@artikel');
	Route::get('/artikel/artikelForm', 'AdminController@artikelForm');
	Route::post('/artikel/artikelCreate', 'AdminController@artikelCreate');
	Route::get('/artikel/artikelFormEdit/{id}', 'AdminController@artikelFormEdit');
	Route::post('/artikel/artikelEdit/{id}', 'AdminController@artikelEdit');
	Route::post('/artikel/artikelDelete/{id}', 'AdminController@artikelDelete');
});
//** END Halaman Admin **//
