<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Artikel extends Model
{
    protected $table = 'artikel';
    protected $fillable = ['judul_artikel', 'isi_artikel', 'foto_artikel', 'tanggal_artikel', 'view_informasi', 'slug'];

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->slug = Str::slug($model->judul_artikel);
        });
    }

    public function getRouteKeyName()
	{
    return 'slug';
	}
}
