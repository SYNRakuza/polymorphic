<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galeri extends Model
{
    protected $table = 'galeri';
    protected $fillable = ['nama_galeri', 'tanggal_galeri', 'foto_galeri', 'deskripsi_galeri'];

    public $timestamps = false;
}
