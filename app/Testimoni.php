<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimoni extends Model
{
    protected $table = 'testimoni';
    protected $fillable = ['nama_testimoni', 'judul_testimoni', 'isi_testimoni', 'foto_testimoni'];

    public $timestamps = false;
}
