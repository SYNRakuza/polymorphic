<?php

namespace App\Http\Controllers;

use Image;
use Carbon\Carbon;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use File;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(){
    	return view('admin.dashboard.index', [
    		'title'				=> 'Admin | dashboard',
    		'nav'				=> 'Produk',
    		'controller'		=> 'admin',]);
    }

//** BEGIN PRODUK **//
    public function produk(){
    	$data_produk = \App\Produk::all();
    	return view('admin.dashboard.produk',[
    		'data_produk'	 	=> $data_produk,
    		'title'				=> 'Admin | List Produk',	
    		'nav'				=> 'Produk',
    		'judul'				=> 'Data Semua Produk',
    		'controller'		=> 'produk']);
    }

    public function produkCreate(UserStoreRequest $request){
    	$validated = $request->validated();
    	if($request->hasFile('foto')) {
        	//get filename with extension
        	$filenamewithextension = $request->file('foto')->getClientOriginalName();
        	//get filename without extension
        	$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        	//get file extension
        	$extension = $request->file('foto')->getClientOriginalExtension();
        	//filename to store
        	$filenametostore = $filename.'_'.time().'.'.$extension;
        	//Upload File
        	$request->file('foto')->storeAs('public/produk_images', $filenametostore);
        	$request->file('foto')->storeAs('public/produk_images/thumbnail', $filenametostore);
        	//Resize image here
        	$thumbnailpath = public_path('storage/produk_images/thumbnail/'.$filenametostore);
        	$img = Image::make($thumbnailpath)->resize(800, 900)->save($thumbnailpath);

        	$dt = Carbon::now();
        	$request->request->add(['tanggal_produk' => $dt]);
    		$request->request->add(['foto_produk' => $filenametostore]);
    		$produk = \App\Produk::create($request->all());
        	return redirect('/produk')->with('sukses', "Produk Berhasil di Tambahkan.");
    	}
    }

    public function produkEdit(UserUpdateRequest $request, $id){
    	$produk = \App\Produk::find($id);

    	if($request->hasFile('foto')) {
    		$validated = $request->validated();
    		//get filename with extension
        	$filenamewithextension = $request->file('foto')->getClientOriginalName();
        	//get filename without extension
        	$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        	//get file extension
        	$extension = $request->file('foto')->getClientOriginalExtension();
        	//filename to store
        	$filenametostore = $filename.'_'.time().'.'.$extension;
        	//delete existing file
        	$foto_lama = $request->foto_lama;
        	$destinationPath = 'storage/produk_images';
        	$destinationPathThumbnail = 'storage/produk_images/thumbnail';
 			File::delete($destinationPath.'/'.$foto_lama);
 			File::delete($destinationPathThumbnail.'/'.$foto_lama);
        	//Upload File
        	$request->file('foto')->storeAs('public/produk_images', $filenametostore);
        	$request->file('foto')->storeAs('public/produk_images/thumbnail', $filenametostore);
        	//Resize image here
        	$thumbnailpath = public_path('storage/produk_images/thumbnail/'.$filenametostore);
        	$img = Image::make($thumbnailpath)->resize(800, 900)->save($thumbnailpath);

    		$request->request->add(['foto_produk' => $filenametostore]);
    		$produk->update($request->all());
        	return redirect('/produk')->with('sukses', "Produk Berhasil di Perbaharui.");
    	}
    	else{
    		$produk->update($request->all());
    		return redirect('/produk')->with('sukses', "Produk Berhasil di Perbaharui.");
    	}
    }

    public function produkDelete(Request $request, $id){
    	//delete existing file
        $foto_lama = $request->foto;
        $destinationPath = 'storage/produk_images';
        $destinationPathThumbnail = 'storage/produk_images/thumbnail';
 		File::delete($destinationPath.'/'.$foto_lama);
 		File::delete($destinationPathThumbnail.'/'.$foto_lama);

    	$produk = \App\Produk::find($id);
    	$produk->delete();

    	return redirect('/produk')->with('sukses', "Produk Berhasil di Hapus.");
    }
//** END PRODUK **//
//** BEGIN GALERI **//
    public function galeri(){
    	$data_galeri = \App\Galeri::all();
    	return view('admin.dashboard.galeri',[
    		'data_galeri'	 	=> $data_galeri,
    		'title'				=> 'Admin | List Galeri',	
    		'nav'				=> 'Galeri',
    		'judul'				=> 'Data Semua Galeri',
    		'controller'		=> 'galeri']);
    }

    public function galeriCreate(UserStoreRequest $request){
    	$validated = $request->validated();
    	if($request->hasFile('foto')) {
        	//get filename with extension
        	$filenamewithextension = $request->file('foto')->getClientOriginalName();
        	//get filename without extension
        	$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        	//get file extension
        	$extension = $request->file('foto')->getClientOriginalExtension();
        	//filename to store
        	$filenametostore = $filename.'_'.time().'.'.$extension;
        	//Upload File
        	$request->file('foto')->storeAs('public/galeri_images', $filenametostore);
        	$request->file('foto')->storeAs('public/galeri_images/thumbnail', $filenametostore);
        	//Resize image here
        	$thumbnailpath = public_path('storage/galeri_images/thumbnail/'.$filenametostore);
        	$img = Image::make($thumbnailpath)->resize(400, 400)->save($thumbnailpath);

        	$dt = Carbon::now();
        	$request->request->add(['tanggal_galeri' => $dt]);
    		$request->request->add(['foto_galeri' => $filenametostore]);
    		$produk = \App\Galeri::create($request->all());
        	return redirect('/galeri')->with('sukses', "Galeri Berhasil di Tambahkan.");
    	}

    }

    public function galeriEdit(UserUpdateRequest $request, $id){
    	$galeri = \App\Galeri::find($id);

    	if($request->hasFile('foto')) {
    		$validated = $request->validated();
    		//get filename with extension
        	$filenamewithextension = $request->file('foto')->getClientOriginalName();
        	//get filename without extension
        	$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        	//get file extension
        	$extension = $request->file('foto')->getClientOriginalExtension();
        	//filename to store
        	$filenametostore = $filename.'_'.time().'.'.$extension;
        	//delete existing file
        	$foto_lama = $request->foto_lama;
        	$destinationPath = 'storage/galeri_images';
        	$destinationPathThumbnail = 'storage/galeri_images/thumbnail';
 			File::delete($destinationPath.'/'.$foto_lama);
 			File::delete($destinationPathThumbnail.'/'.$foto_lama);
        	//Upload File
        	$request->file('foto')->storeAs('public/galeri_images', $filenametostore);
        	$request->file('foto')->storeAs('public/galeri_images/thumbnail', $filenametostore);
        	//Resize image here
        	$thumbnailpath = public_path('storage/galeri_images/thumbnail/'.$filenametostore);
        	$img = Image::make($thumbnailpath)->resize(400, 400)->save($thumbnailpath);

    		$request->request->add(['foto_galeri' => $filenametostore]);
    		$galeri->update($request->all());
        	return redirect('/galeri')->with('sukses', "Galeri Berhasil di Perbaharui.");
    	}
    	else{
    		$galeri->update($request->all());
    		return redirect('/galeri')->with('sukses', "Galeri Berhasil di Perbaharui.");
    	}
    }

    public function galeriDelete(Request $request, $id){
    	//delete existing file
        $foto_lama = $request->foto;
        $destinationPath = 'storage/galeri_images';
        $destinationPathThumbnail = 'storage/galeri_images/thumbnail';
 		File::delete($destinationPath.'/'.$foto_lama);
 		File::delete($destinationPathThumbnail.'/'.$foto_lama);

    	$galeri = \App\Galeri::find($id);
    	$galeri->delete();

    	return redirect('/galeri')->with('sukses', "Galeri Berhasil di Hapus.");
    }

//** END GALERI **//
//** BEGIN PROMO **//
    public function promo(){
    	$data_promo = \App\Promo::all();
    	return view('admin.dashboard.promo',[
    		'data_promo'	 	=> $data_promo,
    		'title'				=> 'Admin | List Promo',	
    		'nav'				=> 'Promo',
    		'judul'				=> 'Data Semua Promo',
    		'controller'		=> 'promo']);
    }

    public function promoCreate(Request $request){
    	$promo = \App\Promo::create($request->all());
    	return redirect('/promo')->with('sukses', "Promo Berhasil di Tambahkan.");
    }

    public function promoEdit(Request $request, $id){
    	$promo = \App\Promo::find($id);
    	$promo->update($request->all());

    	return redirect('/promo')->with('sukses', "Promo Berhasil di Update");
    }
 
    public function promoDelete(Request $request, $id){
    	$promo = \App\Promo::find($id);
    	$promo->delete();

    	return redirect('/promo')->with('sukses', "Promo Berhasil di Hapus.");
    }
//** END PROMO **//
//** BEGIN TESTIMONI **//
    public function testimoni(){
    	$data_testi = \App\Testimoni::all();
    	return view('admin.dashboard.testimoni',[
    		'data_testi'	 	=> $data_testi,
    		'title'				=> 'Admin | List Testimoni',	
    		'nav'				=> 'Testimoni',
    		'judul'				=> 'Data Semua Testimoni',
    		'controller'		=> 'testimoni']);
    }

    public function testimoniCreate(UserStoreRequest $request){
    	if($request->hasFile('foto')) {
    		$validated = $request->validated();
        	//get filename with extension
        	$filenamewithextension = $request->file('foto')->getClientOriginalName();
        	//get filename without extension
        	$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        	//get file extension
        	$extension = $request->file('foto')->getClientOriginalExtension();
        	//filename to store
        	$filenametostore = $filename.'_'.time().'.'.$extension;
        	//Upload File
        	$request->file('foto')->storeAs('public/testimoni_images', $filenametostore);
        	$request->file('foto')->storeAs('public/testimoni_images/thumbnail', $filenametostore);
        	//Resize image here
        	$thumbnailpath = public_path('storage/testimoni_images/thumbnail/'.$filenametostore);
        	$img = Image::make($thumbnailpath)->resize(200, 200)->save($thumbnailpath);

    		$request->request->add(['foto_testimoni' => $filenametostore]);
    		$testimoni = \App\Testimoni::create($request->all());
        	return redirect('/testimoni')->with('sukses', "Testimoni Berhasil di Tambahkan.");
    	}
    	else{
    		$testimoni = \App\Testimoni::create($request->all());
    		return redirect('/testimoni')->with('sukses', "Testimoni Berhasil di Tambahkan.");
    	}
    }

    public function testimoniEdit(UserUpdateRequest $request, $id){
    	$testimoni = \App\Testimoni::find($id);

    	if($request->hasFile('foto')) {
    		$validated = $request->validated();
    		//get filename with extension
        	$filenamewithextension = $request->file('foto')->getClientOriginalName();
        	//get filename without extension
        	$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        	//get file extension
        	$extension = $request->file('foto')->getClientOriginalExtension();
        	//filename to store
        	$filenametostore = $filename.'_'.time().'.'.$extension;
        	//delete existing file
        	$foto_lama = $request->foto_lama;
        	$destinationPath = 'storage/testimoni_images';
        	$destinationPathThumbnail = 'storage/testimoni_images/thumbnail';
 			File::delete($destinationPath.'/'.$foto_lama);
 			File::delete($destinationPathThumbnail.'/'.$foto_lama);
        	//Upload File
        	$request->file('foto')->storeAs('public/testimoni_images', $filenametostore);
        	$request->file('foto')->storeAs('public/testimoni_images/thumbnail', $filenametostore);
        	//Resize image here
        	$thumbnailpath = public_path('storage/testimoni_images/thumbnail/'.$filenametostore);
        	$img = Image::make($thumbnailpath)->resize(200, 200)->save($thumbnailpath);

    		$request->request->add(['foto_testimoni' => $filenametostore]);
    		$testimoni->update($request->all());
        	return redirect('/testimoni')->with('sukses', "Testimoni Berhasil di Perbaharui.");
    	}
    	else{
    		$testimoni->update($request->all());
    		return redirect('/testimoni')->with('sukses', "Testimoni Berhasil di Perbaharui.");
    	}
    }

    public function testimoniDelete(Request $request, $id){
    	//delete existing file
        $foto_lama = $request->foto;
        $destinationPath = 'storage/testimoni_images';
        $destinationPathThumbnail = 'storage/testimoni_images/thumbnail';
 		File::delete($destinationPath.'/'.$foto_lama);
 		File::delete($destinationPathThumbnail.'/'.$foto_lama);

    	$testimoni = \App\Testimoni::find($id);
    	$testimoni->delete();

    	return redirect('/testimoni')->with('sukses', "Testimoni Berhasil di Hapus.");
    }
//** END TESTIMONI **//
//** BEGIN PESAN **//
    public function pesan(){
    	$data_pesan = \App\Pesan::all();
    	return view('admin.dashboard.pesan',[
    		'data_pesan'	 	=> $data_pesan,
    		'title'				=> 'Admin | List Pesan',	
    		'nav'				=> 'Pesan',
    		'judul'				=> 'Semua Pesan Masuk',
    		'controller'		=> 'pesan']);
    }

    public function pesanDelete(Request $request, $id){
    	$pesan = \App\Pesan::find($id);
    	$pesan->delete();

    	return redirect('/pesan')->with('sukses', "Pesan Berhasil di Hapus.");
    }
//** END PESAN **//
//** BEGIN ADMIN **//
    public function user(){
    	$data_admin = \App\User::all();
    	return view('admin.dashboard.admin',[
    		'data_admin'	 	=> $data_admin,
    		'title'				=> 'Admin | List Admin',	
    		'nav'				=> 'Admin',
    		'judul'				=> 'Data Semua Admin',
    		'controller'		=> 'admin']);
    }

    public function userCreate(UserStoreRequest $request){
    	$user = new \App\User;
    	if($request->hasFile('foto')) {
    		$validated = $request->validated();
        	//get filename with extension
        	$filenamewithextension = $request->file('foto')->getClientOriginalName();
        	//get filename without extension
        	$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        	//get file extension
        	$extension = $request->file('foto')->getClientOriginalExtension();
        	//filename to store
        	$filenametostore = $filename.'_'.time().'.'.$extension;
        	//Upload File
        	$request->file('foto')->storeAs('public/admin_images', $filenametostore);
        	//Resize image here
        	$imagepath = public_path('storage/admin_images/'.$filenametostore);
        	$img = Image::make($imagepath)->resize(90, 90)->save($imagepath);

    		$user->name = $request->name;
    		$user->username = $request->username;
    		$user->foto = $filenametostore;
    		$user->password = bcrypt($request->password);
    		$user->remember_token = Str::random(60);
    		$user->save();
        	return redirect('/user')->with('sukses', "Admin Berhasil di Tambahkan.");
    	}
    	else{
    		$user->name = $request->name;
    		$user->username = $request->username;
    		$user->password = bcrypt($request->password);
    		$user->remember_token = Str::random(60);
    		$user->save();
    		return redirect('/user')->with('sukses', "Admin Berhasil di Tambahkan.");
    	}
    }

    public function userEdit(UserUpdateRequest $request, $id){
    	$user = \App\User::find($id);
    	if($request->hasFile('foto')) {
    		$validated = $request->validated();
        	//get filename with extension
        	$filenamewithextension = $request->file('foto')->getClientOriginalName();
        	//get filename without extension
        	$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        	//get file extension
        	$extension = $request->file('foto')->getClientOriginalExtension();
        	//filename to store
        	$filenametostore = $filename.'_'.time().'.'.$extension;
        	//Upload File
        	$request->file('foto')->storeAs('public/admin_images', $filenametostore);
        	//Resize image here
        	$imagepath = public_path('storage/admin_images/'.$filenametostore);
        	$img = Image::make($imagepath)->resize(90, 90)->save($imagepath);
        		if($request->password != '') {
        		$user->update([
                	'name'              => $request->name,
                	'password'          => bcrypt($request->password),
                	'username'          => $request->username,
                	'foto'              => $filenametostore,
                ]);
        		}
        		else{
        		$user->update([
        			'name'				=> $request->name,
        			'password'			=> $request->pass_lama,
        			'username'			=> $request->username,
        			'foto'              => $filenametostore,
        		]);
        		}
    	}
    	else{
    		if($request->password != '') {
        		$user->update([
                	'name'              => $request->name,
                	'password'          => bcrypt($request->password),
                	'username'          => $request->username,
                	'foto'              => $request->foto_lama,
                ]);
        		}
        		else{
        		$user->update([
        			'name'				=> $request->name,
        			'password'			=> $request->pass_lama,
        			'username'			=> $request->username,
        			'foto'              => $request->foto_lama,
        		]);
        		}
    	}
    	return redirect('/user')->with('sukses', "Admin Berhasil di Perbaharui.");
    }

    public function userDelete(Request $request, $id){
    	//delete existing file
        $foto_lama = $request->foto;
        $destinationPath = 'storage/admin_images';
 		File::delete($destinationPath.'/'.$foto_lama);

    	$admin = \App\User::find($id);
    	$admin->delete();

    	return redirect('/user')->with('sukses', "Admin Berhasil di Hapus.");
    }
//** END ADMIN **//
//** BEGIN ARRIKEL **//
    public function artikel(){
    	$data_artikel = \App\Artikel::all();
    	return view('admin.dashboard.artikel',[
    		'data_artikel'	 	=> $data_artikel,
    		'title'				=> 'Admin | List Artikel',	
    		'nav'				=> 'Artikel',
    		'judul'				=> 'Daftar Semua Artikel',
    		'controller'		=> 'artikel']);
    }

    public function artikelForm(){
        return view('admin.dashboard.createArtikel',[
            'title'             => 'Admin | Tambah Informasi',
            'judul'             => 'Tambah Artikel',
            'nav'               => 'Tambah Informasi',
            'controller'        => 'artikel'
            ]);
    }

    public function artikelCreate(UserStoreRequest $request){
        $validated = $request->validated();
        if($request->hasFile('foto')) {
            //get filename with extension
            $filenamewithextension = $request->file('foto')->getClientOriginalName();
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            //get file extension
            $extension = $request->file('foto')->getClientOriginalExtension();
            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;
            //Upload File
            $request->file('foto')->storeAs('public/artikel_images', $filenametostore);
            $request->file('foto')->storeAs('public/artikel_images/thumbnail', $filenametostore);
            $request->file('foto')->storeAs('public/artikel_images/thumbnail2', $filenametostore);
            //Resize image here
            $thumbnailpath = public_path('storage/artikel_images/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(300, 300)->save($thumbnailpath);

            $thumbnailpath2 = public_path('storage/artikel_images/thumbnail2/'.$filenametostore);
            $img2 = Image::make($thumbnailpath2)->resize(482, 264)->save($thumbnailpath2);

            $dt = Carbon::now();
            $request->request->add(['tanggal_artikel' => $dt]);
            $request->request->add(['foto_artikel' => $filenametostore]);
            $artikel = \App\Artikel::create($request->all());
            return redirect('/artikel')->with('sukses', "Artikel Berhasil di Tambahkan.");
        }
    }

    public function artikelFormEdit($id){
        $data_artikel = \App\Artikel::where('id', $id)->get();
        return view('admin.dashboard.editArtikel',[
            'data_artikel'      => $data_artikel,
            'title'             => 'Admin | Edit Informasi',
            'judul'             => 'Edit Artikel',
            'nav'               => 'Edit Artikel',
            'controller'        => 'artikel'
            ]);
    }

    public function artikelEdit(UserUpdateRequest $request, $id){
        $artikel = \App\Artikel::find($id);

        if($request->hasFile('foto')) {
            $validated = $request->validated();
            //get filename with extension
            $filenamewithextension = $request->file('foto')->getClientOriginalName();
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            //get file extension
            $extension = $request->file('foto')->getClientOriginalExtension();
            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;
            //delete existing file
            $foto_lama = $request->foto_lama;
            $destinationPath = 'storage/artikel_images';
            $destinationPathThumbnail = 'storage/artikel_images/thumbnail';
            $destinationPathThumbnail2 = 'storage/artikel_images/thumbnail2';
            File::delete($destinationPath.'/'.$foto_lama);
            File::delete($destinationPathThumbnail.'/'.$foto_lama);
            File::delete($destinationPathThumbnail2.'/'.$foto_lama);
            //Upload File
            $request->file('foto')->storeAs('public/artikel_images', $filenametostore);
            $request->file('foto')->storeAs('public/artikel_images/thumbnail', $filenametostore);
            $request->file('foto')->storeAs('public/artikel_images/thumbnail2', $filenametostore);
            //Resize image here
            $thumbnailpath = public_path('storage/artikel_images/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(300, 300)->save($thumbnailpath);

            $thumbnailpath2 = public_path('storage/artikel_images/thumbnail2/'.$filenametostore);
            $img2 = Image::make($thumbnailpath2)->resize(482, 264)->save($thumbnailpath2);

            $request->request->add(['foto_artikel' => $filenametostore]);
            $artikel->update($request->all());
            return redirect('/artikel')->with('sukses', "Artikel Berhasil di Perbaharui.");
        }
        else{
            $artikel->update($request->all());
            return redirect('/artikel')->with('sukses', "Artikel Berhasil di Perbaharui.");
        }
    }

    public function artikelDelete(Request $request, $id){
        //delete existing file
        $foto_lama = $request->foto;
        $destinationPath = 'storage/artikel_images';
        $destinationPathThumbnail = 'storage/artikel_images/thumbnail';
        $destinationPathThumbnail2 = 'storage/artikel_images/thumbnail2';
        File::delete($destinationPath.'/'.$foto_lama);
        File::delete($destinationPathThumbnail.'/'.$foto_lama);
        File::delete($destinationPathThumbnail2.'/'.$foto_lama);

        $artikel = \App\Artikel::find($id);
        $artikel->delete();

        return redirect('/artikel')->with('sukses', "Artikel Berhasil di Hapus.");
    }
//** END ARTIKEL **//
}
