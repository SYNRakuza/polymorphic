<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Str;

class HomeController extends Controller
{
 	public function index(){
        $data_popular = \App\Artikel::latest('view_informasi')->take(1)->get();
        $data_baru = \App\Artikel::latest('id')->take(3)->get();
        $data_promo =\App\Promo::all();
        $data_galeri = \App\Galeri::all();
        $data_testimoni = \App\Testimoni::all();
    	return view('home.polymorphic.index', [
            'data_testimoni'    => $data_testimoni,
            'data_galeri'       => $data_galeri,
            'data_promo'        => $data_promo,
            'data_baru'         => $data_baru,
            'data_popular'      => $data_popular,
    		'title'				=> 'Polymorphic']);
    }

    public function produk(){
        $data_produk = \App\Produk::all();
    	return view('home.polymorphic.produk', [
            'data_produk'       => $data_produk,
    		'title'				=> 'Polymorphic | Produk']);
    }

    public function artikel(){
        $data_artikel = \App\Artikel::latest('id')->paginate(4);
    	return view('home.polymorphic.artikel', [
            'data_artikel'      => $data_artikel,
    		'title'				=> 'Polymorphic | Artikel']);
    }

    public function selengkapnya($slug){
        $data_view = \App\Artikel::where('slug', $slug)->increment('view_informasi');
        $data_artikel = \App\Artikel::where('slug', $slug)->get();
        return view('home.polymorphic.single', [
            'data_artikel'      => $data_artikel,
            'title'             => 'Polymorphic | Artikel']);
    }

    public function kontak(){
        return view('home.polymorphic.kontak', [
            'title'             => 'Polymorphic | Kontak'
            ]);
    }

    public function kontakKirim(Request $request){
        $dt = Carbon::now();
        $request->request->add(['waktu_saran' => $dt]);
        $kontak = \App\Pesan::create($request->all());
        return redirect('/home/kontak')->with('sukses', "Pesan Berhasil Di Kirim.");
    }   
}