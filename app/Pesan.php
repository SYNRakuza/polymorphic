<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesan extends Model
{
    protected $table = 'pesan';
    protected $fillable = ['nama_pesan', 'email_pesan', 'no_wa_pesan', 'perihal_pesan', 'isi_pesan', 'waktu_saran'];

    public $timestamps = false;
}
