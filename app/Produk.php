<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk';
    protected $fillable = ['nama_produk', 'tanggal_produk', 'foto_produk', 'deskripsi_produk'];

    public $timestamps = false;
}
