<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Promo extends Model
{
    protected $table = 'promo';
    protected $fillable = ['nama_promo', 'start_promo', 'end_promo', 'deskripsi_promo'];

    public function setStartPromoAttribute($start_promo) {
        $this->attributes['start_promo'] = Carbon::createFromFormat('d/m/Y', $start_promo)->format('Y-m-d');
    }

    public function setEndPromoAttribute($end_promo) {
        $this->attributes['end_promo'] = Carbon::createFromFormat('d/m/Y', $end_promo)->format('Y-m-d');
    }

    public $timestamps = false;
}
