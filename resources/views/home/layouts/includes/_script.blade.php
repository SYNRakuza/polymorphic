	<!-- Necessary-JavaScript-Files-&-Links -->

		<!-- Bootstrap-JavaScript-File -->  <script type="text/javascript" src="{{asset('assets/home/js/bootstrap.min.js')}}"></script>
		<!-- Supportive-JavaScript-File --> <script type="text/javascript" src="{{asset('assets/home/js/jquery-2.1.4.min.js')}}"></script>

		<!-- Navigation-Menu-JavaScript -->
			<script src="{{asset('assets/home/js/classie.js')}}"></script>
			<script src="{{asset('assets/home/js/main3.js')}}"></script>
		<!-- //Navigation-Menu-JavaScript -->
		@if(request()->is('/'))
		<!-- Popup-Box-JavaScript -->
			<script src="{{asset('assets/home/js/modernizr.custom.97074.js')}}"></script>
			<script src="{{asset('assets/home/js/jquery.chocolat.js')}}"></script>
			<script type="text/javascript">
				$(function() {
					$('.gallery-item a').Chocolat();
				});
			</script>
		<!-- //Popup-Box-JavaScript -->
		
		<!-- FlexSlider-JavaScript -->
			<script defer src="{{asset('assets/home/js/jquery.flexslider.js')}}"></script>
			<script type="text/javascript">
				$(function(){
					SyntaxHighlighter.all();
				});
				$(window).load(function(){
					$('.flexslider').flexslider({
						animation: "slide",
						start: function(slider){
							$('body').removeClass('loading');
						}
					});
				});
			</script>
		<!-- //FlexSlider-JavaScript -->
		@endif
		@if(request()->is('home/produk*'))
		<!-- Products-Popup-JavaScript -->
			<script src="{{asset('assets/home/js/jquery.swipebox.min.js')}}"></script> 
				<script type="text/javascript">
					jQuery(function($) {
						$(".swipebox").swipebox();
					});
			</script>
		<!-- //Products-Popup-JavaScript -->
		@endif

	<!-- //Necessary-JavaScript-Files-&-Links -->



</body>
<!-- //Body -->

</html>