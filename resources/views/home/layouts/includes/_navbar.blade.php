<!-- Body -->
<body>

	<div class="container">

		<div class="menu-wrap">

			<!-- Navigation-Menu -->
			<nav class="menu">
				<div class="icon-list">
					<a href="/"><span class="glyphicon glyphicon-home" aria-hidden="true"></span><span>BERANDA</span></a>
					<a href="/home/produk"><span class="glyphicon glyphicon-gift" aria-hidden="true"></span><span>PRODUK KAMI</span></a>
					<a href="/home/artikel"><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span><span>ARTIKEL</span></a>
					<a href="/home/kontak"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span><span>KONTAK</span></a>
				</div>
			</nav>
			<!-- //Navigation-Menu -->

			<!-- Close-Button -->
			<button class="close-button" id="close-button">Close Menu</button>
			<!-- //Close-Button -->

			<div class="morph-shape" id="morph-shape" data-morph-open="M0,100h1000V0c0,0-136.938,0-224,0C583,0,610.924,0,498,0C387,0,395,0,249,0C118,0,0,0,0,0V100z">
				<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1000 100" preserveAspectRatio="none">
					<path d="M0,100h1000l0,0c0,0-136.938,0-224,0c-193,0-170.235-1.256-278-35C399,34,395,0,249,0C118,0,0,100,0,100L0,100z"/>
				</svg>
			</div>

		</div>

		<!-- Menu-Button -->
		<button class="menu-button" id="open-button">Open Menu</button>
		<!-- //Menu-Button -->

		<a href="https://wa.widget.web.id/953009" target="_blank" class="wawhatsapp"><img src="{{asset('assets/home/images/whatsappbtn.png')}}"></a>