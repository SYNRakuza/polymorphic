<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->



<!DOCTYPE html>
<html>

<!-- Head -->
<head>

	<title>{{$title}}</title>
	<!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/home/images/logo.png')}}">

	<!-- Meta-Tags -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="keywords" content="Polymorphic, Sablon, Makassar, Clothing, Desain Baju, Baju">
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- //Meta-Tags -->

	<!-- Custom-Stylesheet-Links -->
		<!-- Bootstrap-CSS -->		 <link rel="stylesheet" href="{{asset('assets/home/css/bootstrap.min.css')}}" 	type="text/css" media="all">
		<!-- Default-CSS -->		 <link rel="stylesheet" href="{{asset('assets/home/css/style.css')}}" 			type="text/css" media="all">
		<!-- Navigation-Menu-CSS --> <link rel="stylesheet" href="{{asset('assets/home/css/menu_wave.css')}}" 		type="text/css" media="all">
		<!-- Gallery-Popup-CSS --> 	 <link rel="stylesheet" href="{{asset('assets/home/css/chocolat.css')}}"			type="text/css" media="all">
		<link rel="stylesheet" href="{{asset('assets/home/css/swipebox.css')}}"			type="text/css" media="all">
	<!-- //Custom-Stylesheet-Links -->

	<!-- Navigation-Menu-JavaScript --> <script src="{{asset('assets/home/js/snap.svg-min.js')}}"></script>
	<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Specialities-Hover-Effect-JavaScript --> <script src="{{asset('assets/home/js/modernizr.custom.js')}}"></script>

	<!-- Web-Fonts -->
		<!-- Body-Font -->	 <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700"					type='text/css'>
		<!-- Logo-Font -->	 <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Yeseva+One"									type='text/css'>
		<!-- Link-Font -->	 <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Raleway:400,200,300,500,600,700,800,900"		type='text/css'>
	<!-- //Web-Fonts -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

</head>
<!-- //Head -->