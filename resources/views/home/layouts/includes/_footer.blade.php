				<!-- Footer -->
				<div class="footer">
					<div class="container">

						<div class="footer-grids">

							<div class="col-md-3 col-sm-3 footer-grid footer-grid-1 address">
								<h4>Alamat</h4>
								<address>
									<ul>
										<li><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> BTN Berua Indah Blok A12/2</li>
										<li><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Kelurahan Paccerakkang</li>
										<li><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Kecamatan Biringkanaya</li>
										<li><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Makassar</li>
									</ul>
								</address>
							</div>

							<div class="col-md-4 col-sm-4 footer-grid footer-grid-2 phone">
								<h4>Telepon</h4>
								<p><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> 082347525443</p><br>
								<h4>Email</h4>
								<p><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> <a href="mailto: polymorphic.official@gmail.com"> polymorphic.official@gmail.com</a></p>
							</div>
							<!-- Newsletter -->
							<div class="col-md-5 col-sm-5 footer-grid footer-grid-4 newsletter">
								<h4>Halaman Facebook</h4>
								<div class="fb-page" data-href="https://www.facebook.com/Polymorphic_official-103320131174327/" data-tabs="timeline,events,messages" data-width="350" data-height="190" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Polymorphic_official-103320131174327/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Polymorphic_official-103320131174327/">Polymorphic_official</a></blockquote></div>
							</div>
							<!-- //Newsletter -->
							<div class="clearfix"></div>
							
						</div>

						<!-- Copyright -->
						<div class="copyright">
							<p>&copy; 2020 Polymorphic. All Rights Reserved</p>
						</div>
						<!-- //Copyright -->

					</div>
				</div>
				<!-- //Footer -->

			</div>
			<!-- //Page-Content -->

		</div>

	</div>
	<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v5.0"></script>