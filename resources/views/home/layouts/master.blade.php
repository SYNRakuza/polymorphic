@include('home.layouts.includes._header')

@include('home.layouts.includes._navbar')

@yield('content')

@include('home.layouts.includes._footer')

@include('home.layouts.includes._script')