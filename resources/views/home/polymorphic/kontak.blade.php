@extends('home.layouts.master')  
        @section('content')
<div class="content-wrap">

			<!-- Page-Content -->
			<div class="content">

				<!-- Header-1 -->
				<div class="header-1">
					<!-- Logo -->
					<div class="logo">
						<a href="/"><img style="max-width: 25%;height: auto;" src="{{asset('assets/home/images/logo 2.png')}}"></a>
					</div>
					<!-- //Logo -->

					<h1>KONTAK</h1>
				</div>
				<!-- //Header-1 -->

				<!-- Contact-Info -->
				<div class="footer footer-1" style="background-color:#323232;color: #fff;">
					<div class="container">

						<h3>HUBUNGI KAMI</h3><br>

						<div class="contact-hny-form mt-lg-5 mt-3">
                        <h5 class="title-hny mb-lg-5 mb-3" style="color: green;">
                        @if($message = Session::get('sukses'))
            			<div class="alert alert-success" role="alert">
                		{{$message}}
            			</div>
            			@endif
                        </h5>
                        <form action="/kontak/kirim" method="post">
                        {{csrf_field()}}
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" name="nama_pesan" placeholder="Nama Lengkap" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="email_pesan" placeholder="Email" id="w3lSender" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="number" name="no_wa_pesan" placeholder="Telepon" id="w3lSender">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="perihal_pesan" placeholder="Perihal" id="w3lSubject" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <textarea type="text" name="isi_pesan" placeholder="Pesan Anda" id="w3lMessage" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group" style="text-align:center;">
                                    <button type="submit" class="sbm">Kirim Pesan</button>
                                </div>
                            </div>
                        </form>
                    </div>

					</div>
				</div>
				<!-- //Contact-Info -->

				<!-- Contact-Banner -->
				<div class="contact-banner">

					<!-- Social-Icons -->
					<div class="social">
						<h2>Ikuti Kami</h2>
						<ul class="social-icons">
							<li><a href="https://www.facebook.com/Polymorphic_official-103320131174327/" class="facebook" title="Go to Our Facebook Page"></a></li>
							<li><a href="https://www.instagram.com/polymorphic.id/" class="instagram" title="Go to Our Instagram Account"></a></li>
						</ul>
					</div>
					<!-- //Social-Icons -->

					<div class="contact-banner-grids">
						<div class="col-md-4 col-sm-4 contact-grid contact-grid-1">
							<p><a href="#map"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></a></p>
							<h3>Temukan Kami di Maps</h3>
						</div>
						<div class="col-md-4 col-sm-4 contact-grid contact-grid-2">
							<p><a href="https://wa.widget.web.id/953009"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></a></p>
							<h3>Hubungi Kami Lewat Whatsapp</h3>
						</div>
						<div class="col-md-4 col-sm-4 contact-grid contact-grid-3">
							<p><a href="mailto:polymorphic.official@gmail.com?subject=subject&cc=cc@gmail.com"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></a></p>
							<h3>Subscribe Kami</h3>
						</div>
						<div class="clearfix"></div>
					</div>

				</div>
				<!-- //Contact-Banner -->

				<!-- Contact -->
				
				<!-- //Contact -->


      	@endsection