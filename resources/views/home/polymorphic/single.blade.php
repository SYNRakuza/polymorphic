@extends('home.layouts.master')  
        @section('content')
        <div class="content-wrap">

			<!-- Page-Content -->
			<div class="content">

				<!-- Header-1 -->
				<div class="header-1">
					<!-- Logo -->
					<div class="logo">
						<a href="/"><img style="max-width: 25%;height: auto;" src="{{asset('assets/home/images/logo 2.png')}}"></a>
					</div>
					<!-- //Logo -->

					<a href="/home/artikel/"><h1>ARTIKEL</h1></a>
				</div>
				<!-- News -->
				<div class="news">
					<div class="container">
						@foreach($data_artikel as $artikel)
						<div class="col-md-10 col-sm-10 fashion" style="float:none; margin:0 auto;">
						<h3 style="color:white">{{$artikel->judul_artikel}}</h3>
							<img src="{{asset('storage/artikel_images/'.$artikel->foto_artikel)}}" alt="Couture">
							{!! $artikel->isi_artikel!!}<br>
							<div class="meta">
								<i class="fa fa-user" style="color:lightblue;" aria-hidden="true"></i><span style="color:lightblue; font-size:16px;"> Polymorphic</span>
								<i class="fa fa-calendar" aria-hidden="true"></i><span style="font-size:14px;"> {{\Carbon\Carbon::parse($artikel->tanggal_artikel)->format('d/m/Y')}}</span>
							</div>
						</div><br>
						@endforeach
						<div style="text-align:center;">
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
@endsection