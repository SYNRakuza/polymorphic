@extends('home.layouts.master')  
        @section('content')
		<div class="content-wrap">

			<!-- Page-Content -->
			<div class="content">

				<!-- Header-1 -->
				<div class="header-1">
					<!-- Logo -->
					<div class="logo">
						<a href="/"><img style="max-width: 25%;height: auto;" src="{{asset('assets/home/images/logo 2.png')}}"></a>
					</div>
					<!-- //Logo -->

					<h1>PRODUK KAMI</h1>
				</div>
				<!-- //Header-1 -->

				<!-- Collection -->
<!-- 				<div class="collection">
					<div class="container">

						<div class="collection-grids">

							<div class="col-md-4 col-sm-4 collection-left">
								<img src="{{asset('assets/home/images/collection-1.jpg')}}" alt="Couture">
							</div>

							<div class="col-md-8 col-sm-8 collection-right">
								<div class="col-md-4 col-sm-4 collection-grid collection-text collection-grid-1">
									<img src="{{asset('assets/home/images/special-1.png')}}" alt="Couture">
									<h3>Spring</h3>
									<h4>A elementum ligula lacus ac quam ultrices a scelerisque praesent vel suspendisse scelerisque a aenean hac montes.</h4>
								</div>
								<div class="col-md-4 col-sm-4 collection-grid collection-image collection-grid-2">
									<img src="{{asset('assets/home/images/collection-2.jpg')}}" alt="Couture">
								</div>
								<div class="col-md-4 col-sm-4 collection-grid collection-text collection-grid-3">
									<img src="{{asset('assets/home/images/special-2.png')}}" alt="Couture">
									<h3>Summer</h3>
									<h4>A elementum ligula lacus ac quam ultrices a scelerisque praesent vel suspendisse scelerisque a aenean hac montes.</h4>
								</div>
								<div class="col-md-4 col-sm-4 collection-grid collection-image collection-grid-4">
									<img src="{{asset('assets/home/images/collection-3.jpg')}}" alt="Couture">
								</div>
								<div class="col-md-4 col-sm-4 collection-grid collection-text collection-grid-5">
									<img src="{{asset('assets/home/images/special-3.png')}}" alt="Couture">
									<h3>Winter</h3>
									<h4>A elementum ligula lacus ac quam ultrices a scelerisque praesent vel suspendisse scelerisque a aenean hac montes.</h4>
								</div>
								<div class="col-md-4 col-sm-4 collection-grid collection-image collection-grid-6">
									<img src="{{asset('assets/home/images/collection-4.jpg')}}" alt="Couture">
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>

						</div>
						
					</div>
				</div> -->
				<!-- //Collection -->



				<!-- products -->
				<div class="products" id="products">
				@foreach($data_produk as $produk)
						<div class="col-md-3 col-sm-3 gal-left">
							<div class="content-grid-effect slow-zoom vertical text-center">
								<a href="{{asset('storage/produk_images/'.$produk->foto_produk)}}" class="b-link-stripe b-animate-go  swipebox">

									<div class="img-box">
										<img src="{{asset('storage/produk_images/thumbnail/'.$produk->foto_produk)}}" alt="image" class="img-responsive zoom-img">	
									</div>
							
									<div class="info-box">
										<div class="info-content">
											<span class="separator">{{$produk->deskripsi_produk}}</span>
										</div>
									</div>
								</a>
								<h4 style="font: cursive;">{{$produk->nama_produk}}</h2>
							</div>
						</div>
				@endforeach
						<div class="clearfix"></div>
				</div>
				<!-- //products -->
@endsection