@extends('home.layouts.master')  
        @section('content')
        <div class="content-wrap">

			<!-- Page-Content -->
			<div class="content">

				<!-- Header-1 -->
				<div class="header-1">
					<!-- Logo -->
					<div class="logo">
						<a href="/"><img style="max-width: 25%;height: auto;" src="{{asset('assets/home/images/logo 2.png')}}"></a>
					</div>
					<!-- //Logo -->

					<h1>ARTIKEL</h1>
				</div>
				<!-- News -->
				<div class="news">
					<div class="container">
						@foreach($data_artikel as $artikel)
						<div class="col-md-8 col-sm-8 fashion" style="float:none; margin:0 auto;">
						<a href="/blog/{{$artikel->slug}}"><h3>{{$artikel->judul_artikel}}</h3></a>
							<img src="{{asset('storage/artikel_images/thumbnail2/'.$artikel->foto_artikel)}}" alt="Couture">
							{!! Str::limit($artikel->isi_artikel, 600, ' ........')!!}<br>
							<span><a class="readmore" href="/blog/{{$artikel->slug}}">Selengkapnya...</a></span><br>
							<div class="meta">
								<i class="fa fa-user" style="color:lightblue;" aria-hidden="true"></i><span style="color:lightblue; font-size:16px;"> Polymorphic</span>
								<i class="fa fa-calendar" aria-hidden="true"></i><span style="font-size:14px;"> {{\Carbon\Carbon::parse($artikel->tanggal_artikel)->format('D / d M Y')}}</span>
							</div>
						</div><br>
						@endforeach
						<div style="text-align:center;">
						{{$data_artikel->links()}}
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
@endsection