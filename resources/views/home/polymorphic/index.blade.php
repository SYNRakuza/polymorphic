@extends('home.layouts.master')  
        @section('content')
		<div class="content-wrap">

			<!-- Page-Content -->
			<div class="content">

				<!-- Banner -->
				<div class="banner" id="home">
					<img src="{{asset('assets/home/images/banner.jpg')}}" alt="Couture">

					<!-- Logo -->
					<div class="logo">
						<a href="/"><img style="max-width: 25%;height: auto;" src="{{asset('assets/home/images/logo 2.png')}}"></a>
					</div>
					<!-- //Logo -->

					<h1>MILLENIAL CLOTHES</h1>
					<P>Be Whatever You Want</P>
					<a class="browse" href="/home/produk">PRODUK KAMI</a>

				</div>
				<!-- //Banner -->
				
				<!-- Index-About -->
				<div class="index-about">

					<div class="col-md-6 col-sm-6 index-about-grid index-about-info">
						<h1>TENTANG KAMI</h1>
						<P>Polymorpic adalah sebuah perusahan yang bergerak di bidang industri penjualan kaos distro dan percetkan sablon. Kami menerima pesanan kaos, hoodie, jersey, jaket,dan topi. Kami mengandalkan kualiatas dan produksi yang cepat serta promo menarik yang kami tawarkan. Sangat cocok untuk : kampus, kelas, kaos angkatan, baju porseni, penggalangan dana, organisasi, komunitas, event, acara, kantor, pegawai, brand clothing, distro, dll.</P>
					</div>

					<div class="col-md-6 col-sm-6 index-about-grid index-about-image">
						<div class="about-image-1">
							<img src="{{asset('assets/home/images/about-1.jpg')}}" alt="Couture">
						</div>
						<div class="about-image-2">
							<img src="{{asset('assets/home/images/about-2.jpg')}}" alt="Couture">
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
					
				</div>
				<!-- //Index-About -->

				<!-- Speciality -->
				<div class="speciality">

					<div class="col-md-4 col-sm-4 specialities">
						<h2>KEUNGGULAN KAMI</h2>
						<h4>POLYMORPHIC : Millenial Screen Printing</h4>
						<p class="p1">Kami menawarkan keunggulan-keunggulan yang tentunya akan menjadi solusi bagi anda yang ingin mencari sablon kaos terbaik di Makassar.</p>
						<p class="p2">Kami mempunyai 6 keunggulan utama yang akan membuat anda puas sebagai pelanggan kami.</p>
					</div>

					<div class="col-md-8 col-sm-8 grid cs-style-3">
						<div class="col-md-4 col-sm-4 speciality-grid grid1">
							<figure>
								<div class="speciality-info">
									<img src="{{asset('assets/home/images/special-1.png')}}" alt="Couture">
									<h3>Kapasitas Produksi Memadai</h3>
								</div>
								<div class="speciality-hover">
									<figcaption>
										<p style="font-size:14px;">Dapat Mencetak Hingga Ribuan Pcs/bulan</p>
									</figcaption>									
								</div>
							</figure>
						</div>
						<div class="col-md-4 col-sm-4 speciality-grid grid2">
							<figure>
								<div class="speciality-info">
									<img src="{{asset('assets/home/images/special-2.png')}}" alt="Couture">
									<h3>Murah</h3>
								</div>
								<figcaption>
									<p style="font-size:14px;">Harga Dapat Menyesuaikan Dari Banyaknya Pesanan.</p>
								</figcaption>
							</figure>
						</div>
						<div class="col-md-4 col-sm-4 speciality-grid grid3">
							<figure>
								<div class="speciality-info">
									<img src="{{asset('assets/home/images/special-3.png')}}" alt="Couture">
									<h3>Berkualitas</h3>
								</div>
								<figcaption>
									<p style="font-size:14px;">Menggunakan Bahan 100% Cotton Combed Grade A dan Tinta Plastisol.</p>
								</figcaption>
							</figure>
						</div>
						<div class="col-md-4 col-sm-4 speciality-grid grid4">
							<figure>
								<div class="speciality-info">
									<img src="{{asset('assets/home/images/special-4.png')}}" alt="Couture">
									<h3>Proses Cepat</h3>
								</div>
								<figcaption>
									<p style="font-size:14px;">Untuk Produk Kaos Bisa Selesai Dalam 3 Hari.</p>
								</figcaption>
							</figure>
						</div>
						<div class="col-md-4 col-sm-4 speciality-grid grid5">
							<figure>
								<div class="speciality-info">
									<img src="{{asset('assets/home/images/special-5.png')}}" alt="Couture">
									<h3>Desain Gratis</h3>
								</div>
								<figcaption>
									<p style="font-size:14px;">Gambar/File Dalam Bentuk Apapun Akan Kami Desain Ulang.</p>
								</figcaption>
							</figure>
						</div>
						<div class="col-md-4 col-sm-4 speciality-grid grid6">
							<figure>
								<div class="speciality-info">
									<img src="{{asset('assets/home/images/special-6.png')}}" alt="Couture">
									<h3>Admin Responsif</h3>
								</div>
								<figcaption>
									<p style="font-size:14px;">Kami Melayani Pelanggan Dengan Respon yang Cepat.</p>
								</figcaption>
							</figure>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>

				</div>
				<!-- //Speciality -->

				<!-- Events -->
				<div class="events">

					<h3>PROMO SAAT INI</h3>

					<div class="event-grids">
					@foreach($data_promo as $promo)
						<div class="event-grid event-grid-1">
							<div class="event-date">
								<p class="date-1">{{\Carbon\Carbon::parse($promo->start_promo)->format('d')}}-{{\Carbon\Carbon::parse($promo->end_promo)->format('d')}}</p>
								<p style="text-transform: uppercase;" class="date-2">{{\Carbon\Carbon::parse($promo->start_promo)->format('M')}}-{{\Carbon\Carbon::parse($promo->end_promo)->format('M')}}</p>
								<p class="date-3">{{\Carbon\Carbon::parse($promo->end_promo)->format('Y')}}</p> 
								<!-- <p class="date-1">{{\Carbon\Carbon::parse($promo->start_promo)->format('d')}}-{{\Carbon\Carbon::parse($promo->end_promo)->format('d')}}</p>
								<p class="date-2">{{\Carbon\Carbon::parse($promo->start_promo)->format('M')}}-{{\Carbon\Carbon::parse($promo->end_promo)->format('M')}}</p>
								<p class="date-3">{{\Carbon\Carbon::parse($promo->start_promo)->format('Y')}}-{{\Carbon\Carbon::parse($promo->end_promo)->format('Y')}}</p> -->
							</div>
							<div class="event-info">
								<h4>{{$promo->nama_promo}}</h4>
								<P>{!!$promo->deskripsi_promo!!}</P>
							</div>
							<div class="clearfix"></div>
						</div><br>
					@endforeach
						<div class="clearfix"></div>

					</div>
				</div>
				<!-- //Events -->

				<!-- How -->
				<div class="how">

					<div class="how-intro">
						<h3>Petunjuk Pemesanan</h3>
						<div class="col-md-8 col-sm-8 how-heading">
							<p>Untuk proses pemesanan kami akan tetap memberikan kemudahan bagi pelanggan, cara order sangat mudah, silahkan kunjungi workshop/tempat produksi kami atau order via WA kapan saja dan dimana saja.</p>
						</div>

						<!-- <div class="col-md-4 col-sm-4 how-link">
							<a href="about.html"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span> Learn More About Us</a>
						</div> -->
						<div class="clearfix"></div>
					</div>

					<div class="how-grids">

						<div class="how-top-grids">
							<div class="col-md-3 col-sm-3 how-top-grid how-grid-1">
								<div class="how-grid-image">
									<img src="{{asset('assets/home/images/how-1.png')}}" alt="Couture">
								</div>
							</div>
							<div class="col-md-3 col-sm-3 how-top-grid how-grid-2">
								<h4>Siapkan Desain</h4>
								<p>Anda harus menyiapkan desain yang anda pesan, Kami akan memberikan masukan tentang hasil akhir dari cetakan sablon Anda.</p>
							</div>
							<div class="col-md-3 col-sm-3 how-top-grid how-grid-3">
								<div class="how-grid-image">
									<img src="{{asset('assets/home/images/how-2.png')}}" alt="Couture">
								</div>
							</div>
							<div class="col-md-3 col-sm-3 how-top-grid how-grid-4">
								<h4>Nego Harga</h4>
								<p>Setelah melihat desain Anda, kami akan menentukan harga pembuatan kaos. Setelah harga disepakati, Anda wajib menyerahkan DP sebesar 60%</p>
							</div>
							<div class="clearfix"></div>
						</div>

						<div class="how-bottom-grids">
							<div class="col-md-3 col-sm-3 how-top-grid how-grid-5">
								<h4>Pengerjaan</h4>
								<p>Setelah kami menerima DP, maka kami akan segera mengerjakan pembuatan. Durasi waktu proses pengerjaan minimal 3 hari (kami bekerja sesuai dengan antrian cetakan orderan).</p>
							</div>
							<div class="col-md-3 col-sm-3 how-top-grid how-grid-6">
								<div class="how-grid-image">
									<img src="{{asset('assets/home/images/how-3.png')}}" alt="Couture">
								</div>
							</div>
							<div class="col-md-3 col-sm-3 how-top-grid how-grid-7">
								<h4>Selesai</h4>
								<p>Jika orderan anda telah selesai kami akan segera menghubungi Anda, walau belum sampai pada waktu yang di tentukan. pesanan diserahkan kepada Anda jika telah melakukan pelunasan.</p>
							</div>
							<div class="col-md-3 col-sm-3 how-top-grid how-grid-8">
								<div class="how-grid-image">
									<img src="{{asset('assets/home/images/how-4.png')}}" alt="Couture">
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
						
					</div>
					
				</div>
				<!-- //How -->

				<!-- Gallery -->
				<div class="gallery">

					<div class="follow">
						<h3>GALERI</h3>
						<h4>Dokumentasi & Galeri</h4>
						<img src="{{asset('assets/home/images/follow.jpg')}}" alt="Couture">
						<!-- Social-Icons -->
						<div class="social">
							<ul class="social-icons">
								<li><a href="https://www.facebook.com/Polymorphic_official-103320131174327/" class="facebook" title="Go to Our Facebook Page"></a></li>
								<li><a href="https://www.instagram.com/polymorphic.id/" class="instagram" class="instagram" title="Go to Our Instagram Account"></a></li>
							</ul>
						</div>
						<!-- //Social-Icons -->
						<div class="clearfix"></div>
					</div>

					<div class="gallery-grids">
						<div class="gallery-items">
							@foreach($data_galeri as $galeri)
							<div class="col-md-3 col-sm-3 gallery-item gallery-item-1">
								<a class="example-image-link" href="{{asset('storage/galeri_images/'.$galeri->foto_galeri)}}" data-lightbox="example-set" data-title="">
									<div class="grid">
										<figure class="effect-apollo">
											<img src="{{asset('storage/galeri_images/thumbnail/'.$galeri->foto_galeri)}}" alt="Manufactory">
												<figcaption style="font:Lucida Bright;color:#485692"></figcaption>
										</figure>
									</div>
								</a>
							</div>
							@endforeach
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="clearfix"></div>
					
				</div>
				<!-- //Gallery -->

				<!-- News -->
				<div class="news">
					<div class="container">

						<div class="col-md-6 col-sm-6 fashion">
							<h3>ARTIKEL TERPOPULER</h3>
							@foreach($data_popular as $popular)
							<a href="/blog/{{$popular->slug}}"><img src="{{asset('storage/artikel_images/thumbnail2/'.$popular->foto_artikel)}}" alt="Couture"></a>
							<a href="/blog/{{$popular->slug}}"><h4>{{$popular->judul_artikel}}</h4></a>
							<p>{!! Str::limit($popular->isi_artikel, 830, ' .....')!!}</p>
							
						</div>
						@endforeach

						<div class="col-md-6 col-sm-6 latest-news">
							<a href="/home/artikel"><h3>ARTIKEL TERBARU</h3></a>
								<div class="event-grids">
								@foreach($data_baru as $baru)
									<a href="/blog/{{$baru->slug}}"><div class="event-grid event-grid-1">
										<div class="event-image">
											<img src="{{asset('storage/artikel_images/thumbnail/'.$baru->foto_artikel)}}" alt="Couture">
										</div>
										<div class="event-info">
											<h4>{{$baru->judul_artikel}}</h4>
											<h5>{{\Carbon\Carbon::parse($baru->tanggal_artikel)->format('d M Y')}}</h5>
											<P>{!! Str::limit($baru->isi_artikel, 280, ' ...')!!}</P>
										</div>
										<div class="clearfix"></div>
									</div></a><br>
								@endforeach
									<div class="clearfix"></div>

								</div>
						</div>
						<div class="clearfix"></div>

					</div>
				</div>
				<!-- //News -->

				<!-- Clients -->
				<div class="clients" id="clients">
					<div class="container">

						<h3>TESTIMONI</h3>

						<section class="slider">
							<div class="flexslider">
								<ul class="slides">
								@foreach($data_testimoni as $testimoni)
									<li>
										<div class="client-image">
											<img src="{{asset('storage/testimoni_images/thumbnail/'.$testimoni->foto_testimoni)}}" alt="Couture">
											<h5>- {{$testimoni->nama_testimoni}}</h5>
										</div>
										<div class="client-info">
											<h4>"{{$testimoni->judul_testimoni}}"</h4>
											<p>{{$testimoni->isi_testimoni}}</p>
										</div>
									</li>
								@endforeach
								</ul>
							</div>
						</section>

					</div>
				</div>
				<!-- //Clients -->
@endsection