@extends('admin.layouts.master')  
        @section('content')        
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin">Admin</a></li>
                        <li class="breadcrumb-item active"><a href="/{{$controller}}">{{$nav}}</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->

            <div class="container-fluid">
            @if($message = Session::get('sukses'))
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
            @endif
            @foreach ($errors->all() as $message)
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
            @endforeach
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">{{$judul}}</h4>
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal" >Tambah<span class="btn-icon-right"><i class="fa fa-plus-square"></i></span>
                                </button>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Promo</th>
                                                <th>Deskripsi Promo</th>
                                                <th>Tanggal Mulai</th>
                                                <th>Tanggal Berakhir</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 1 ?>
                                        @foreach($data_promo as $promo)
                                            <tr>
                                                <td style="vertical-align: middle;text-align: center;">{{$no++}}</td>
                                                <td style="vertical-align: middle;text-align: left;">{{$promo->nama_promo}}</td>
                                                <td style="vertical-align: middle;text-align: left;">{{$promo->deskripsi_promo}}</td>
                                                <td style="vertical-align: middle;text-align: center;">{{ \Carbon\Carbon::parse($promo->start_promo)->format('d/m/Y')}}</td>
                                                <td style="vertical-align: middle;text-align: center;">{{ \Carbon\Carbon::parse($promo->end_promo)->format('d/m/Y')}}</td>
                                                <td style="vertical-align: middle;text-align: center;">
                                                    <button type="button" class="btn mb-1 btn-primary btn-md" data-toggle="modal" data-target="#editModal{{$promo->id}}"><i class="fa fa-pencil"></i>
                                                    </button>
                                                    <button type="button" class="btn mb-1 btn-danger btn-md" data-toggle="modal" data-target="#deleteModal{{$promo->id}}" ><i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
<!-- Modal ADD DATA -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Promo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="Post" action="/promo/promoCreate">
                {{csrf_field()}}
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Nama Promo</label>
                        <input type="text" class="form-control" name="nama_promo" required> 
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Deskripsi</label>
                        <textarea class="summernote" id="message-text" name="deskripsi_promo" required></textarea>
                    </div>
                    <label for="message-text" class="col-form-label">Durasi Promo</label>
                    <div class="input-daterange input-group" id="date-range">
                        <input type="text" class="form-control" name="start_promo"> <span class="input-group-addon bg-info b-0 text-white">To</span>
                        <input type="text" class="form-control" name="end_promo">
                    </div>
                    <div class="form-group">
                        <label for="message"> </label>
                    </div>
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Tambah Promo</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal ADD DATA -->

<!-- Modal EDIT DATA -->
@foreach($data_promo as $promo)
<div class="modal fade" id="editModal{{$promo->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Promo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="Post" action="/promo/promoEdit/{{$promo->id}}">
                {{csrf_field()}}
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Nama Promo</label>
                        <input type="text" class="form-control" name="nama_promo" value="{{$promo->nama_promo}}" required> 
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Deskripsi</label>
                        <textarea class="form-control summernote" id="message-text" name="deskripsi_promo" required>{{$promo->deskripsi_promo}}</textarea>
                    </div>
                    <label for="message-text" class="col-form-label">Durasi Promo</label>
                    <div class="input-daterange input-group" id="date-range">
                        <input type="text" class="form-control" name="start_promo" value="{{ \Carbon\Carbon::parse($promo->start_promo)->format('d/m/Y')}}"> <span class="input-group-addon bg-info b-0 text-white">To</span>
                        <input type="text" class="form-control" name="end_promo" value="{{ \Carbon\Carbon::parse($promo->end_promo)->format('d/m/Y')}}">
                    </div>
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Update Promo</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
<!-- End Modal EDIT DATA -->

<!-- Delete Data -->
@foreach($data_promo as $promo)
<div class="modal fade" id="deleteModal{{$promo->id}}" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Data</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <form method="post" action="/promo/promoDelete/{{$promo->id}}">
            {{csrf_field()}}
            <div class="modal-body">Apakah anda yakin menghapus data anda?</div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Hapus</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
<!-- End Delete -->
@endsection