@extends('admin.layouts.master')  
        @section('content')        
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin">Admin</a></li>
                        <li class="breadcrumb-item active"><a href="/{{$controller}}">{{$nav}}</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->

            <div class="container-fluid">
            @if($message = Session::get('sukses'))
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
            @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">{{$judul}}</h4>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Perihal</th>
                                                <th>Tanggal Masuk</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 1 ?>
                                        @foreach($data_pesan as $pesan)
                                            <tr>
                                                <td style="vertical-align: middle;text-align: center;">{{$no++}}</td>
                                                <td style="vertical-align: middle;text-align: left;">{{$pesan->nama_pesan}}</td>
                                                 <td style="vertical-align: middle;text-align: center;">{{$pesan->email_pesan}}</td>
                                                <td style="vertical-align: middle;text-align: left;">{{$pesan->perihal_pesan}}</td>
                                                <td style="vertical-align: middle;text-align: left;">{{$pesan->waktu_saran}}</td>
                                                <td style="vertical-align: middle;text-align: center;">
                                                    <button type="button" class="btn mb-1 btn-primary btn-md" data-toggle="modal" data-target="#detailModal{{$pesan->id}}"><i class="fa fa-envelope-open"></i>
                                                    </button>
                                                    <button type="button" class="btn mb-1 btn-danger btn-md" data-toggle="modal" data-target="#deleteModal{{$pesan->id}}" ><i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

<!-- Modal DETAIL DATA -->
@foreach($data_pesan as $pesan)
<div class="modal fade" id="detailModal{{$pesan->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pesan Masuk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="Post" action="">
                {{csrf_field()}}
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Nama</label>
                        <input type="text" class="form-control" name="nama_testimoni" value="{{$pesan->nama_pesan}}" readonly> 
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Email</label>
                        <input type="text" class="form-control" name="email_pesan" value="{{$pesan->email_pesan}}" readonly> 
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">No. WA</label>
                        <input type="text" class="form-control" name="no_wa_pesan" value="{{$pesan->no_wa_pesan}}" readonly> 
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Perihal</label>
                        <input type="text" class="form-control" name="perihal_pesan" value="{{$pesan->perihal_pesan}}" readonly> 
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Isi Pesan</label>
                        <textarea class="form-control" id="message-text" name="isi_pesan" readonly>{{$pesan->isi_pesan}}</textarea>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal DETAIL DATA -->
@endforeach

<!-- Delete Data -->
@foreach($data_pesan as $pesan)
<div class="modal fade" id="deleteModal{{$pesan->id}}" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Pesan</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <form method="post" action="/pesan/pesanDelete/{{$pesan->id}}">
            {{csrf_field()}}
            <div class="modal-body">Apakah anda yakin menghapus pesan ini?</div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Hapus</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
<!-- End Delete -->
@endsection