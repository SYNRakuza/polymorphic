@extends('admin.layouts.master')  
        @section('content')        
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin">Admin</a></li>
                        <li class="breadcrumb-item active"><a href="/{{$controller}}">{{$nav}}</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->

            <div class="container-fluid">
            @if($message = Session::get('sukses'))
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
            @endif
            @foreach ($errors->all() as $message)
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
            @endforeach
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">{{$judul}}</h4>
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal" >Tambah<span class="btn-icon-right"><i class="fa fa-plus-square"></i></span>
                                </button>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Galeri</th>
                                                <th>Gambar Galeri</th>
                                                <th>Deskripsi Galeri</th>
                                                <th>Tanggal Posting</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $no = 1 ?>
                                        @foreach($data_galeri as $galeri)
                                            <tr>
                                                <td style="vertical-align: middle;text-align: center;">{{$no++}}</td>
                                                <td style="vertical-align: middle;text-align: left;">{{$galeri->nama_galeri}}</td>
                                                <td style="vertical-align: middle;text-align: center;"><img src="{{asset('storage/galeri_images/thumbnail/'.$galeri->foto_galeri)}}" class="img-responsive" style="max-height: 240px; max-width: 200px;"></td>
                                                <td style="vertical-align: middle;text-align: left;">{{$galeri->deskripsi_galeri}}</td>
                                                <td style="vertical-align: middle;text-align: center;">{{ \Carbon\Carbon::parse($galeri->tanggal_galeri)->format('d/m/Y')}}</td>
                                                <td style="vertical-align: middle;text-align: center;">
                                                    <button type="button" class="btn mb-1 btn-primary btn-md" data-toggle="modal" data-target="#editModal{{$galeri->id}}"><i class="fa fa-pencil"></i>
                                                    </button>
                                                    <button type="button" class="btn mb-1 btn-danger btn-md" data-toggle="modal" data-target="#deleteModal{{$galeri->id}}" ><i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
<!-- Modal ADD DATA -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Galeri</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="Post" action="/galeri/galeriCreate" enctype="multipart/form-data">
                {{csrf_field()}}
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Nama Galeri</label>
                        <input type="text" class="form-control" name="nama_galeri" required> 
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Gambar</label>
                        <input type="file" class="form-control-file" name="foto" accept=".png, .jpg, .jpeg" required>
                        <div style="font-size: 10px">File hanya JPG dan PNG dengan ukuran Maks. 2048 Kb</div> 
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Deskripsi</label>
                        <textarea class="summernote" id="message-text" name="deskripsi_galeri" required></textarea>
                    </div>
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Tambah Galeri</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal ADD DATA -->

<!-- Modal EDIT DATA -->
@foreach($data_galeri as $galeri)
<div class="modal fade" id="editModal{{$galeri->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Galeri</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="Post" action="/galeri/galeriEdit/{{$galeri->id}}" enctype="multipart/form-data">
                {{csrf_field()}}
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Nama Galeri</label>
                        <input type="text" class="form-control" name="nama_galeri" value="{{$galeri->nama_galeri}}" required> 
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Gambar</label>
                        <input type="file" class="form-control-file" name="foto" accept=".png, .jpg, .jpeg">
                        <div style="font-size: 10px">File hanya JPG dan PNG dengan ukuran Maks. 2048 Kb</div>
                        <img src="{{asset('storage/galeri_images/thumbnail/'.$galeri->foto_galeri)}}" class="img-responsive" style="max-height: 240px; max-width: 200px;"> 
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Deskripsi</label>
                        <textarea class="summernote" id="message-text" name="deskripsi_galeri" required>{{$galeri->deskripsi_galeri}}</textarea>
                    </div>
                    <input type="hidden" name="foto_lama" value="{{$galeri->foto_galeri}}">
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Update Galeri</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
<!-- End Modal EDIT DATA -->

<!-- Delete Data -->
@foreach($data_galeri as $galeri)
<div class="modal fade" id="deleteModal{{$galeri->id}}" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Data</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                </button>
            </div>
            <form method="post" action="/galeri/galeriDelete/{{$galeri->id}}">
            {{csrf_field()}}
            <div>
                <input type="hidden" class="form-control" name="foto" value="{{$galeri->foto_galeri}}">
            </div>
            <div class="modal-body">Apakah anda yakin menghapus data anda?</div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger">Hapus</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endforeach
<!-- End Delete -->
@endsection