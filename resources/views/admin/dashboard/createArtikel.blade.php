@extends('admin.layouts.master')  
        @section('content')    
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="row page-titles mx-0">
                <div class="col p-md-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin">Admin</a></li>
                        <li class="breadcrumb-item active"><a href="/{{$controller}}">{{$nav}}</a></li>
                    </ol>
                </div>
            </div>
            <!-- row -->

            <div class="container-fluid">
            @if($message = Session::get('sukses'))
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
            @endif
            @foreach ($errors->all() as $message)
            <div class="alert alert-success" role="alert">
                {{$message}}
            </div>
            @endforeach
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">{{$judul}}</h4>
                                <div class="basic-form">
                                    <form method="POST" action="/artikel/artikelCreate" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label>Judul Artikel</label>
                                                <input type="text" name="judul_artikel" maxlength="200" class="form-control" placeholder="Masukkan Judul Artikel"  required>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-8">
                                                <label>Content</label>
                                                <textarea class="form-control" style="height: 200px;" name="isi_artikel"></textarea>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Cover</label>
                                                <input type="file" name="foto" maxlength="40" class="form-control" required }}>
                                                <div style="font-size: 10px">File hanya JPG dan PNG dengan ukuran Maks. 2048 Kb</div>
                                            </div>

                                            <div class="form-group col-md-4">
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <button type="submit" class="btn btn-success col-md-12">Tambah Artikel</button>
                                        </div>  
                                    </form>
                                 </div>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
@endsection