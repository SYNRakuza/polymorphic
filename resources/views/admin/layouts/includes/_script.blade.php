        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright &copy; Designed & Developed by <a href="https://themeforest.net/user/quixlab">Quixlab</a> 2018</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="{{asset('assets/admin/plugins/common/common.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/custom.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/settings.js')}}"></script>
    <script src="{{asset('assets/admin/js/gleek.js')}}"></script>
    <script src="{{asset('assets/admin/js/styleSwitcher.js')}}"></script>
    <script src="{{asset('assets/admin/js/costum.js')}}"></script>
    <script src="{{asset('assets/admin/js/jquery-3.4.1.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/tables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/tables/js/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/tables/js/datatable-init/datatable-basic.min.js')}}"></script>

    <script src="{{asset('assets/admin/plugins/summernote/dist/summernote.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/summernote/dist/summernote-init.js')}}"></script>
    @if(request()->is('promo*'))
    <script src="{{asset('assets/admin/plugins/moment/moment.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="{{asset('assets/admin/plugins/clockpicker/dist/jquery-clockpicker.min.js')}}"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="{{asset('assets/admin/plugins/jquery-asColorPicker-master/libs/jquery-asColor.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/jquery-asColorPicker-master/libs/jquery-asGradient.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js')}}"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="{{asset('assets/admin/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="{{asset('assets/admin/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/admin/js/plugins-init/form-pickers-init.js')}}"></script>
    @endif
</body>

</html>