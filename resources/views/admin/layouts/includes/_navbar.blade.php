<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo">
                <a href="/">
                    <b class="logo-abbr" style="color:white;">P</b>
                    <span class="logo-compact" style="color:white">P</span>
                    <span class="brand-title" style="color:white;">
                        POLYMORPHIC
                    </span>
                </a>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">    
            <div class="header-content clearfix">
                
                <div class="nav-control">
                    <div class="hamburger">
                        <span class="toggle-icon"><i class="icon-menu"></i></span>
                    </div>
                </div>
                <div class="header-left">
                    <div class="input-group icons">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-transparent border-0 pr-2 pr-sm-3" id="basic-addon1"><i class="mdi mdi-magnify"></i></span>
                        </div>
                        <input type="search" class="form-control" placeholder="Search Dashboard" aria-label="Search Dashboard">
                        <div class="drop-down   d-md-none">
                            <form action="#">
                                <input type="text" class="form-control" placeholder="Search">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="header-right">
                    <ul class="clearfix">
                        <li class="icons dropdown">
                            <div class="user-img c-pointer position-relative"   data-toggle="dropdown">
                                <span class="activity active"></span>
                                <img src="{{asset('storage/admin_images/'.auth()->user()->foto)}}" height="40" width="40" alt="">
                            </div>
                            <div class="drop-down dropdown-profile   dropdown-menu">
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="app-profile.html" type="button" data-toggle="modal" data-target="#editModal{{auth()->user()->id}}"><i class="icon-user"></i> <span>Profile</span></a>
                                        </li>
                                        <li><a href="/logout"><i class="icon-key"></i> <span>Logout</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->
<!-- Modal EDIT DATA -->
<div class="modal fade" id="editModal{{auth()->user()->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="Post" action="/user/userEdit/{{auth()->user()->id}}" enctype="multipart/form-data">
                {{csrf_field()}}
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Nama</label>
                        <input type="text" class="form-control" name="name" value="{{auth()->user()->name}}" required> 
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Username</label>
                        <input type="text" class="form-control" name="username" value="{{auth()->user()->username}}" required> 
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Password</label>
                        <input type="password" class="form-control" name="password" placeholder="kosongan jika tidak ingin di ubah"> 
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Foto</label>
                        <input type="file" class="form-control-file" name="foto" accept=".png, .jpg, .jpeg">
                        <div style="font-size: 10px">File hanya JPG dan PNG dengan ukuran Maks. 2048 Kb</div>
                        <img src="{{asset('storage/admin_images/'.auth()->user()->foto)}}" class="img-responsive" style="max-height: 90px; max-width: 90px;"> 
                    </div>
                    <input type="hidden" name="pass_lama" value="{{auth()->user()->password}}">
                    <input type="hidden" name="foto_lama" value="{{auth()->user()->foto}}">
                    <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Update Admin</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modal EDIT DATA -->