        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="nk-sidebar">           
            <div class="nk-nav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="mega-menu mega-menu-sm {{ (request()->is('admin*')) ? 'active' : '' }}">
                        <a href="/admin" aria-expanded="false">
                            <i class="icon-home menu-icon"></i><span class="nav-text">Dashboard</span>
                        </a>
                    </li>
                    <li class="mega-menu mega-menu-sm {{ (request()->is('produk*')) ? 'active' : '' }}">
                        <a href="/produk" aria-expanded="false">
                            <i class="icon-handbag menu-icon "></i><span class="nav-text">Produk</span>
                        </a>
                    </li>
                    <li class="mega-menu mega-menu-sm {{ (request()->is('galeri*')) ? 'active' : '' }}">
                        <a href="/galeri" aria-expanded="false">
                            <i class="icon-picture menu-icon"></i><span class="nav-text">Galeri</span>
                        </a>
                    </li>
                    <li class="mega-menu mega-menu-sm {{ (request()->is('artikel*')) ? 'active' : '' }}">
                        <a href="/artikel" aria-expanded="false">
                            <i class="icon-book-open menu-icon"></i><span class="nav-text">Artikel</span>
                        </a>
                    </li>
                    <li class="mega-menu mega-menu-sm {{ (request()->is('promo*')) ? 'active' : '' }}">
                        <a href="/promo" aria-expanded="false">
                            <i class="icon-present menu-icon"></i><span class="nav-text">Promo</span>
                        </a>
                    </li>
                    <li class="mega-menu mega-menu-sm {{ (request()->is('testimoni*')) ? 'active' : '' }}">
                        <a href="/testimoni" aria-expanded="false">
                            <i class="icon-heart menu-icon"></i><span class="nav-text">Testimoni</span>
                        </a>
                    </li>
                    <li class="mega-menu mega-menu-sm {{ (request()->is('pesan*')) ? 'active' : '' }}">
                        <a href="/pesan" aria-expanded="false">
                            <i class="icon-envelope menu-icon"></i><span class="nav-text">Pesan</span>
                        </a>
                    </li>
                    <li class="mega-menu mega-menu-sm {{ (request()->is('user*')) ? 'active' : '' }}">
                        <a href="/user" aria-expanded="false">
                            <i class="icon-people menu-icon"></i><span class="nav-text">Admin</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->