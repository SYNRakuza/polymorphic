<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>{{$title}}</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/admin/images/favicon.png')}}">
    <!-- Custom Stylesheet -->
    <link href="{{asset('assets/admin/plugins/tables/css/datatable/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/plugins/summernote/dist/summernote.css')}}" rel="stylesheet">

    <!--Text Editor-->
    @if(request()->is('artikel*'))
    <script type="text/javascript" src="https://cloud.tinymce.com/stable/tinymce.min.js"> </script>
		<script type="text/javascript">
			tinyMCE.init({
			         // General options
			         mode : "textareas",
			        // theme : "advanced",
			});
		</script>
    @endif
    @if(request()->is('promo*'))
    <link href="{{asset('assets/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
    <!-- Page plugins css -->
    <link href="{{asset('assets/admin/plugins/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet">
    <!-- Color picker plugins css -->
    <link href="{{asset('assets/admin/plugins/jquery-asColorPicker-master/css/asColorPicker.css')}}" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="{{asset('assets/admin/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <!-- Daterange picker plugins css -->
    <link href="{{asset('assets/admin/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    @endif
</head>